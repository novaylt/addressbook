<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>ADDRESS BOOK</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body id="app-layout">
     <nav class="navbar navbar-default"> <!-- Can change to fixed-top -->

        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
              
                    <!-- Responsive navigation bar on mobile devices -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
           
                <!-- Branding Image -->
  
            </div>
         </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                <ul class="nav navbar-nav">
                @if (Auth::guest())
                @else
                <li><a href="{{ url('/contacts') }}"><i class="fa fa-users" aria-hidden="true"></i><nav-text> CONTACTS</nav-text></a></li>
                <li><a href="{{ url('/create') }}"><i class="fa fa-user-plus" aria-hidden="true"></i><nav-text> CREATE</nav-text></a></li>
                @endif
              

                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i><nav-text> Login</nav-text></a></li>
                            <li><a href="{{ url('/register') }}"><i class="fa fa-book" aria-hidden="true"></i><nav-text> Register</nav-text></a></li>
                        @else
                             <li><a href="#" class="not-active"><i class="fa fa-user" aria-hidden="true"></i><nav-text><strong> Hello {{ Auth::user()->name }}</strong></nav-text></a></li> 
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i><nav-text>Logout</nav-text></a></li>
                          
                        @endif
                        </ul>

            </div>
        </div>
    </nav>

    @yield('content')


<div id="footar" align="center">
                @include('footer')
</div>


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

   
    <script type="text/javascript">
   $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
</script>

</body>
</html>
