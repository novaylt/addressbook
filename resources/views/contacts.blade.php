@extends('layouts.app')

                          <div align="center">
                          <h1><p style="font-family: Impact, fantasy; font-size:32pt;"><strong>#ADDRESS BOOK</strong></p></h1>
                          </div>    



@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-body">
                   

<div class="box-header">
              <h3 class="box-title">Preview All Contacts <small>(<a href='/create'>Enter New Contact</a>)</small></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>
              </div>
            </div>

                     @if(Session::has('contact_delete'))
                             <div class="alert alert-danger">
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-trash" aria-hidden="true"></i> {{ session('contact_delete') }}
                             </div>
                         @endif



                        @if(Session::has('contact_updated'))
                            <div class="alert alert-success">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-check-circle-o" aria-hidden="true"></i> {{ Session::get('contact_updated') }}
                          </div>                  
                         @endif 



  @if (count($AllContacts)) 

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                            <thread>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Telephone</th>
                              <th>Change</th>
                              <th>Manage</th>
                            </tr>
                           </thread>


                             @foreach($AllContacts as $key => $allcontacts)
                             <thread>
                                   <tr>
                                     <td>{{ $allcontacts->fullname }}</th>
                                     <td>{{ $allcontacts->email }}</th>
                                     <td>{{ $allcontacts->telephone }}</th>
                                     <td><a href ="/contacts/edit/{{$allcontacts->id}}" class ='btn btn-info btn-sm'><i class="fa fa-pencil-square-o"></i> Edit</a></th>
                                     <td>{{ Form::open(['route' => ['contacts', $allcontacts->id], 'method' => 'delete']) }}
                                          <input type="hidden" name="_method" value="DELETE">
                                          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                        {{ Form::close() }} 
                                     </th>
                                   </tr>
                                </thread>
                             @endforeach
                         

  @else  
                                 <div align="center">
                                   <br></br>
                                   <strong><h3><b>YOU DO NOT HAVE ANY Contact Entries!</b></h3></strong></p>
                                    <a href ="/create"><p><strong><h3><u>CREATE</u></a> YOUR FIRST CONTACT ENTRY!</h3></strong></p>
                                 </div>
  @endif   

             </table>
                                <div align="center"> 
                                   {!! $AllContacts->render() !!} 
                                </div>
            </div>


</div>
@endsection
