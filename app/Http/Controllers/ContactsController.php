<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Input;
use Redirect;
use Session;
use App\ContactsModel;
use App\Http\Requests;

class ContactsController extends Controller
{
   // Method for creating new contacts to database
  public function store()
  {
      $rules = array(

        'fullname' => 'required',
        'email' => 'required',
        'telephone' => 'required',
      );

      $validator = Validator::make(Input::all(), $rules);
        if ($validator-> fails())
            {
              return redirect('create')
              ->withErrors($validator)
              ->withInput();
            }
        else
            {
              $newcontact = new \App\ContactsModel;
              $newcontact->fullname = Input::get('fullname');
              $newcontact->email = Input::get('email');
              $newcontact->telephone = Input::get('telephone');

              $newcontact -> save();

              Session::flash('contact_saved', 'New Contact Saved!');
              return Redirect::to('create');
            }
    }




    // Method for retrieving and showing all contacts on contacts view
  public function GetContacts(){
    $AllContacts =   \App\ContactsModel::Paginate(2);
    return view('contacts')->with ('AllContacts', $AllContacts);
  }



  // Method for deleting a contact entry on contacts view
    public function DeleteContact($id)
    {
      ContactsModel::destroy($id);

      Session::flash('contact_delete', 'Contact Deleted!');
      return Redirect::to('contacts');
    }


// Method for editing contacts on contacts view
    public function EditContact($id)
    {
      $ContactData = COntactsModel::findOrFail($id);
      return view('edit')->with('ContactData', $ContactData);
    }


    // Method for updating contact details on edit view
    public function UpdateContact($id, Request $request)
    {
       $UpdatedData = ContactsModel::findOrFail($id);

       $this->validate($request, [
          'fullname' => 'required',
          'email' => 'required',
          'telephone' => 'required',
        ]);

        $input = $request->all();
        $UpdatedData->fill($input)->save();

        Session::flash('contact_updated', 'Contact Has Been Updated!');
        return Redirect::to('contacts');
      }

}
