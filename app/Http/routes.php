<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('login');
});


Route::auth();


Route::get('/contacts', function () {
    return view('contacts');
})->middleware(['auth']);

Route::get('contacts', 'ContactsController@GetContacts');
Route::get('contacts/edit/{id}',array('uses' => 'ContactsController@EditContact', 'as' => 'contacts'));
Route::post('contacts/edit/{id}',array('uses' => 'ContactsController@UpdateContact', 'as' => 'contacts'));
Route::delete('contacts/delete/{id}',array('uses' => 'ContactsController@DeleteContact', 'as' => 'contacts'));


Route::get('/create', function () {
    return view('create');
})->middleware(['auth']);

Route::post('create', 'ContactsController@store');



Route::get('/', 'HomeController@index');
