<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsModel extends Model
{
       protected $table = 'contacts';
  	 protected $fillable = array('id','fullname', 'email',  'telephone' , 'created_at', 'updated_at');
}
